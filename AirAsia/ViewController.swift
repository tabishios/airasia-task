//
//  ViewController.swift
//  AirAsia
//
//  Created by Tabish Sohail on 17/11/2019.
//  Copyright © 2019 Tabish Sohail. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

struct Results: Decodable {
    let results: [Places]
    
}

struct Places {
    let geometry: Geometry
    let icon: String?
    let name: String?
}

struct Geometry : Decodable {
    let location: Location
}

struct Location : Decodable {
    let lat : Float?
    let lng : Float?
}

class ViewController: UIViewController {
    static let GOOGLE_API_KEY = "AIzaSyCJ8b4cGtbc4j7xMtkA358zw2_r8Wkro2Y"
    static let annotationImage = "https://maps.gstatic.com/mapfiles/place_api/icons/airport-71.png"
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var goButton: UIButton!
    
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 30000
    var previousLocation: CLLocation?
    
    let geoCoder = CLGeocoder()
    var directionsArray: [MKDirections] = []
    var airports: [Places] = []
    let distanceInMeters: Double = 0.0
    var tappedcoord: CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        goButton.layer.cornerRadius = goButton.frame.size.height/2
        checkLocationServices()
    }
    
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on.
        }
    }
    
    
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            startTackingUserLocation()
        case .denied:
            // Show alert instructing them how to turn on permissions
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            // Show an alert letting them know what's up
            break
        case .authorizedAlways:
            break
        @unknown default:
            break
        }
    }
    
    
    func startTackingUserLocation() {
        mapView.showsUserLocation = true
        centerViewOnUserLocation()
        locationManager.startUpdatingLocation()
//        previousLocation = getCenterLocation(for: mapView)
        
        queryGooglePlaceAirport()

    }
    
    
    func getCenterLocation(for mapView: MKMapView) -> CLLocation {
        
        let latitude = tappedcoord?.latitude
        let longitude = tappedcoord?.longitude
        
        return CLLocation(latitude: latitude!, longitude: longitude!)
    }
    
    
    func getDirections() {
        guard let location = locationManager.location?.coordinate else { return }
        
        let request = createDirectionsRequest(from: location)
        let directions = MKDirections(request: request)
        resetMapView(withNew: directions)
        
        directions.calculate { [unowned self] (response, error) in
            //TODO: Handle error if needed
            guard let response = response else { return } //TODO: Show response not available in an alert
            
            for route in response.routes {
                self.mapView.addOverlay(route.polyline)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
        }
    }
    
    
    func createDirectionsRequest(from coordinate: CLLocationCoordinate2D) -> MKDirections.Request {
        
        let destinationCoordinate       = getCenterLocation(for: mapView).coordinate
        let startingLocation            = MKPlacemark(coordinate: coordinate)
        let destination                 = MKPlacemark(coordinate: destinationCoordinate)
        let request                     = MKDirections.Request()
        
        request.source                  = MKMapItem(placemark: startingLocation)
        request.destination             = MKMapItem(placemark: destination)
        request.transportType           = .automobile
        request.requestsAlternateRoutes = true
        
        return request
    }
    
    
    func resetMapView(withNew directions: MKDirections) {
        mapView.removeOverlays(mapView.overlays)
        directionsArray.append(directions)
        let _ = directionsArray.map { $0.cancel() }
    }
    
    
    @IBAction func goButtonTapped(_ sender: UIButton) {
        getDirections()
//        queryGooglePlaceAirport()
    }
    
    
    func queryGooglePlaceAirport() {
        guard let location = locationManager.location?.coordinate else { return }
        let startingLocation            = MKPlacemark(coordinate: location)

        
        var googleApi = String(format: "https://maps.googleapis.com/maps/api/place/search/json?location=%f,%f&radius=\(20000)&type=airport&sensor=true&key=%@", startingLocation.coordinate.latitude,startingLocation.coordinate.longitude,ViewController.GOOGLE_API_KEY)
        
        print("api-->\(googleApi)")
        
        googleApi = googleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        var urlReq = URLRequest(url: URL(string: googleApi)!)
        
        urlReq.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: urlReq) { (data, response, error) in
            guard error == nil else { return }
            
            guard let data = data else { return }
            
//            let jsonDict = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers )
//            print("json = \(jsonDict ?? "")")
            
            do {
                let places = try JSONDecoder().decode(Results.self, from: data)
                self.airports = places.results
                print("api result->\(self.airports)")
                self.addAnotations()
            } catch {

                print(error)
                // Insert error handling here
            }
            

        }
        task.resume()
    }
    
    
    func addAnotations () {
        
        for airport in airports {
            
            let airportAnnotation = MKPointAnnotation()

            airportAnnotation.title = airport.name
            
            let latitude: CLLocationDegrees = CLLocationDegrees(Float(airport.geometry.location.lat!) )
            let longitude: CLLocationDegrees = CLLocationDegrees(Float(airport.geometry.location.lng!) )
            
            airportAnnotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            DispatchQueue.main.async {
                // Update UI
               self.mapView.addAnnotation(airportAnnotation)

            }
        }
        
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
}

extension ViewController:MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        renderer.strokeColor = .red
        renderer.lineWidth = 2.0
        
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            guard annotation is MKPointAnnotation else { return nil }

        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)

        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true

        } else {
            annotationView!.annotation = annotation
        }

//        guard let urlImg = ViewController.annotationImage else {return annotationView}
        
        guard let imageURL = URL(string: "https://maps.gstatic.com/mapfiles/place_api/icons/airport-71.png") else { return annotationView }
        
        // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            
            let image = UIImage(data: imageData)
            
            DispatchQueue.main.async {
               annotationView?.image = image
            }
        }
        
        annotationView?.isEnabled = true
        annotationView?.canShowCallout = true
        
      
        return annotationView

    }
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        //
        let annotation: MKPointAnnotation = view.annotation as! MKPointAnnotation
        
        tappedcoord = annotation.coordinate;
        
        guard let location = locationManager.location?.coordinate else { return }
        
        let startingLocation = MKPlacemark(coordinate: location)
        
        let coordinate1 = CLLocation(latitude: startingLocation.coordinate.latitude, longitude: startingLocation.coordinate.longitude)
        let coordinate2 = CLLocation(latitude: (tappedcoord?.latitude)!, longitude:(tappedcoord?.longitude)!)
        
        
        let distanceInMeters = coordinate1.distance(from: coordinate2)
        let distanceinKms = distanceInMeters / 1000
        
        let subtitleView = UILabel()
        

        subtitleView.text = String(format: "%.2f Kms", distanceinKms)
        addressLabel.text = String(format: "%.2f Kms", distanceinKms)
        view.detailCalloutAccessoryView = subtitleView
        
        goButton.isHidden = false
    }
    
    
}

extension Places: Decodable {
     enum MyStructKeys: String, CodingKey    {
        case   geometry = "geometry"
        case icon = "icon"
        case name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: MyStructKeys.self)
        
        let name: String = try container.decode(String.self, forKey: .name) // extracting the data
        let icon: String = try container.decode(String.self, forKey: .icon) // extracting the data
        let geometry: Geometry = try container.decode(Geometry.self, forKey: .geometry) // extracting the data
       
        self.init( geometry: geometry,icon: icon, name: name) // initializing our struc

    }
}
